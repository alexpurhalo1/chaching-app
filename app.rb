require 'rubygems'
require 'bundler/setup'

Bundler.require :default, (ENV['RACK_ENV'] || 'development').to_sym

puts ENV['RACK_ENV']

module App
  class API < Grape::API
    format :json

    get '/posts' do
      {posts: [1, 2, 3]}	    
    end
  end
end	
