require './app'
require './lib/process_etag'

use ProcessETag
use Coverband::BackgroundMiddleware
run ActionController::Dispatcher.new
run App::API
