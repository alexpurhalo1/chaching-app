require 'digest'

class ProcessETag
  def initialize(app)
    @app = app
  end
  
  def call(env)
    req = Rack::Request.new(env)
    status, headers, response = @app.call(env)
    etag = Digest::MD5.hexdigest(response.body[0])
    
    if req.get? && req.get_header('HTTP_IF_NONE_MATCH') === etag
      [304, {'Etag' => etag}, []]
    else
      [status, headers.merge('Etag' => etag), response]
    end
  end
end

