require_relative './spec_helper'

describe 'GET /posts' do
  before { get 'posts' }
  it 'returns list of posts' do
    expect(last_response.status).to eq(200)
  end
end

